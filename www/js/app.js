var app = angular.module('app', [
    'ionic'
]);

app.run(function($ionicPlatform, $rootScope, $location) {
    $ionicPlatform.ready(function() {

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        
        if (window.StatusBar) {
            window.StatusBar.overlaysWebView(false);
            window.StatusBar.styleHex('#FFFFFF');
        }

        window.addEventListener('native.keyboardshow', function(){
            document.body.classList.add('keyboard-open');
        });
    });
});