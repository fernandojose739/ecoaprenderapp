app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('top');
  $ionicConfigProvider.backButton.text('').icon('ion-ios7-arrow-left');

  $stateProvider
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
    })
    
    .state('app.painel', {
        url: '/painel',
        views: {
            'menuContent': {
                templateUrl: 'templates/painel.html',
                controller: 'painelController',
                controllerAs: 'painelCtrl'
            }
        }
    })
    
    .state('app.atividades', {
        url: '/atividades',
        views: {
            'menuContent': {
                templateUrl: 'templates/atividades.html',
                controller: 'atividadesController',
                controllerAs: 'atividadesCtrl'
            }
        }
    })
    
    .state('app.atividade', {
        url: '/atividade',
        views: {
            'menuContent': {
                templateUrl: 'templates/atividade.html',
                controller: 'atividadeController',
                controllerAs: 'atividadeCtrl'
            }
        }
    })

    .state('app.comunicados', {
        url: '/comunicados',
        views: {
            'menuContent': {
                templateUrl: 'templates/comunicados.html',
                controller: 'comunicadosController',
                controllerAs: 'comunicadosCtrl'
            }
        }
    })
    
    .state('app.comunicado', {
        url: '/comunicado',
        views: {
            'menuContent': {
                templateUrl: 'templates/comunicado.html',
                controller: 'comunicadoController',
                controllerAs: 'comunicadoCtrl'
            }
        }
    })
    
    .state('app.sugestao', {
        url: '/sugestao',
        views: {
            'menuContent': {
                templateUrl: 'templates/sugestao.html',
                controller: 'sugestaoController',
                controllerAs: 'sugestaoCtrl'
            }
        }
    })
    
    .state('app.videos', {
        url: '/videos',
        views: {
            'menuContent': {
                templateUrl: 'templates/videos.html',
                controller: 'videosController',
                controllerAs: 'videosCtrl'
            }
        }
    })
    
    .state('app.chats', {
        cache: false,
        url: '/chats',
        views: {
            'menuContent': {
                templateUrl: 'templates/chats.html',
                controller: 'chatsController',
                controllerAs: 'chatsCtrl',
            }
        }
    })

    .state('app.chatNovo', {
        url: '/chatNovo',
        views: {
            'menuContent': {
                templateUrl: 'templates/chatNovo.html',
                controller: 'chatNovoController',
                controllerAs: 'chatNovoCtrl',
            }
        }
    })

    .state('app.chatConversa', {
        url: '/chatConversa',
        views: {
            'menuContent': {
                templateUrl: 'templates/chatConversa.html',
                controller: 'chatConversaController',
                controllerAs: 'chatConversaCtrl',
            }
        }
    })
    
    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'usuarioController',
        controllerAs: 'loginCtrl'
    });

  $urlRouterProvider.otherwise('/login');
});
