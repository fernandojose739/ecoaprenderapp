﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('atividadeController', Atividades);

	//Injeção das dependências
	Atividades.$inject = [
        'localStorageService'
	];

	//Construtor
	function Atividades(localStorageService) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;
        
        function setEvents() {	
            ctrl.atividade = JSON.parse(localStorageService.obter('atividade'));		
		}

		function setProperties() {
		}

		function attachEvents() {
		}

		function initialize() {
		}
	}
})();