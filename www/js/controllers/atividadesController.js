﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('atividadesController', Atividades);

	//Injeção das dependências
	Atividades.$inject = [
        'atividadeService',
        'localStorageService',
        '$location',
        '$scope'
	];

	//Construtor
	function Atividades(atividadeService, localStorageService, $location, $scope) {

		var ctrl = this;

		setEvents();
		setProperties();
		attachEvents();
		initialize();

		return ctrl;
        
        function setEvents() {
            ctrl.VerMais = verMais;	
            ctrl.LoadMore = loadMore;		
		}

		function setProperties() {
            ctrl.noMoreItemsAvailable = false;
            ctrl.pagina = -1;
            ctrl.atividades = [];
		}

		function attachEvents() {
		}

		function initialize() {
           
		}
        
        function _listarAtividade() {
			atividadeService.ListarAtividade(ctrl.pagina).then(function(data) {
				ctrl.atividades = ctrl.atividades.concat(data.data);
                
                if (data.data.length <= 0){
                    ctrl.noMoreItemsAvailable = true;
                }
                
                $scope.$broadcast('scroll.infiniteScrollComplete');
			}, function(data) {
				ctrl.noMoreItemsAvailable = true;
                $scope.$broadcast('scroll.infiniteScrollComplete');
			});
		}
        
        function verMais(atividade){
            localStorageService.salvar('atividade', atividade);
            $location.path('/app/atividade')
        }
        
        function loadMore() {
            ctrl.pagina = ctrl.pagina + 1;
             _listarAtividade();
        };
    }
})();