﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('chatsController', chatsController);

	//Injeção das dependências
	chatsController.$inject = [
        'chatService',
        'localStorageService',
        '$location',
        '$scope',
        'usuarioService'
	];

	//Construtor
	function chatsController(chatService, localStorageService, $location, $scope, usuarioService) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;
        
        function setEvents() {
            ctrl.exibirConversa = exibirConversa;	
            ctrl.carregarMais = carregarMais;
		};

		function setProperties() {
            ctrl.noMoreItemsAvailable = false;
            ctrl.page = -1;
            ctrl.chats = [];
            ctrl.alerta = '';
            ctrl.usuario = usuarioService.obterDoLocalStorage();
		};

		function initialize() {
            
		};
        
        function listar() {
            ctrl.alerta = '';
			chatService.list(ctrl.page, ctrl.usuario.UsuarioId).then(function(data) {
				ctrl.chats = ctrl.chats.concat(data.data);
                
                if (ctrl.chats.length <= 0){
                    ctrl.alerta = "Nenhuma mensagem na caixa de entrada";
                }
                
                if (data.data.length <= 0){
                    ctrl.noMoreItemsAvailable = true;
                }
                
                $scope.$broadcast('scroll.infiniteScrollComplete');
			}, function(data) {
				ctrl.noMoreItemsAvailable = true;
                $scope.$broadcast('scroll.infiniteScrollComplete');
	            ctrl.alerta = "Nenhuma mensagem na caixa de entrada";
			});
		};
        
        function exibirConversa(chat){
            localStorageService.salvar('chat', chat);
            $location.path('/app/chatConversa')
        };
        
        function carregarMais() {
            ctrl.page = ctrl.page + 1;
            listar();
        };
    }
})();