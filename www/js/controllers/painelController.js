﻿(function() {
	'use strict';

	//Assinatura da controller
	app.controller('painelController', painelController);

	//Injeção das dependências
	painelController.$inject = [
        '$rootScope',
		'$scope'
	];

	//Construtor
	function painelController($rootScope, $scope) {

		var ctrl = this;

		setEvents();
		setProperties();
		initialize();

		return ctrl;
        
        function setEvents() {
		}

		function setProperties() {
		}

		function initialize() {			
		}
	}
})();