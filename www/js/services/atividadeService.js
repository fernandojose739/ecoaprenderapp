(function () {
	'use strict';

	app.service('atividadeService', AtividadeService);

	AtividadeService.$inject = ['baseService', '$q', 'escolaService'];

	function AtividadeService(baseService, $q, escolaService) {

		var service = this;

		service.ListarAtividade = listarAtividade;

		return service;

		function listarAtividade(pagina) {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "atividade/" + pagina + "/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}
	}
})();