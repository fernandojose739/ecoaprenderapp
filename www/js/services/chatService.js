(function () {
	'use strict';

	app.service('chatService', chatService);

	chatService.$inject = ['baseService', '$q', 'escolaService'];

	function chatService(baseService, $q, escolaService) {

		var service = this;

		service.list = list;
		service.listarMensagens = listarMensagens;
		service.enviar = enviar;
		service.responder = responder;

		return service;

		function list(page, usuarioId) {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "chat/listar/" + page + "/" + usuarioId + '/' + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function listarMensagens(chatId, ultimoId) {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "chat-mensagem/listar/" + chatId + "/" + ultimoId + "/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function enviar(request){
			var escola = escolaService.obterEscola();
			request.EscolaId = escola.Id;

			var def = $q.defer();
			baseService.call("POST", "chat/enviar", request)
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function responder(request){
			var escola = escolaService.obterEscola();
			request.EscolaId = escola.Id;
			
			var def = $q.defer();
			baseService.call("POST", "chat-mensagem/responder", request)
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};
	}
})();