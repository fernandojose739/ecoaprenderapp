(function () {
	'use strict';

	app.service('comunicadoService', ComunicadoService);

	ComunicadoService.$inject = ['baseService', '$q', 'escolaService'];

	function ComunicadoService(baseService, $q, escolaService) {

		var service = this;

		service.ListarTodos = listarTodos;
		service.confirmar = confirmar;
		service.marcarComoLido = marcarComoLido;
		service.listarRelatorio = listarRelatorio;

		return service;

		function listarTodos(pagina) {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "comunicado/" + pagina + "/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function listarRelatorio(comunicadoId) {
			var def = $q.defer();
			baseService.call("GET", "comunicado/relatorio/" + comunicadoId, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function confirmar(request){
			var escola = escolaService.obterEscola();
			request.EscolaId = escola.Id;

			var def = $q.defer();
			baseService.call("POST", "comunicado-confirmar-usuario/confirmar", request)
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function marcarComoLido(request){
			var escola = escolaService.obterEscola();
			request.EscolaId = escola.Id;
			
			var def = $q.defer();
			baseService.call("POST", "comunicado-leitura-usuario/salvar", request)
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};
	}
})();