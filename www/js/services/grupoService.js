(function () {
	'use strict';

	app.service('grupoService', grupoService);

	grupoService.$inject = ['baseService', '$q', 'escolaService'];

	function grupoService(baseService, $q, escolaService) {

		var service = this;

		service.listar = listar;

		return service;

		function listar(pagina) {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "grupo/listar/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}
	}
})();