﻿(function () {
	'use strict';

	app.factory('localStorageService', LocalStorageService);

	LocalStorageService.$inject = ['$window'];

	function LocalStorageService($window) {

		var dataFactory = {};
        
        dataFactory.salvarString = function (chave, valor) {
            $window.localStorage[chave] = valor;            
		};

		dataFactory.salvar = function (chave, valor) {
            $window.localStorage[chave] = JSON.stringify(valor);            
		};

		dataFactory.obter = function (chave) {
			return $window.localStorage[chave];
		}

		dataFactory.remover = function (chave) {
			$window.localStorage.removeItem(chave);
		}

		return dataFactory;

	}
})();