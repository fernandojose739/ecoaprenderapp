(function () {
	'use strict';

	app.service('paraTipoService', paraTipoService);

	paraTipoService.$inject = ['baseService', '$q', 'escolaService'];

	function paraTipoService(baseService, $q) {

		var service = this;

		service.listar = listar;

		return service;

		function listar() {
			var def = $q.defer();
			baseService.call("GET", "para-tipo/listar", "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};
	}
})();