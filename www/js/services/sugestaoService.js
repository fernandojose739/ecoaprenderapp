(function () {
	'use strict';

	app.service('sugestaoService', SugestaoService);

	SugestaoService.$inject = ['baseService', '$q', 'escolaService'];

	function SugestaoService(baseService, $q, escolaService) {

		var service = this;

		service.ListarTodos = listarTodos;
        service.Adicionar = adicionar;
        service.Responder = responder;

		return service;

		function listarTodos(request) {
			var def = $q.defer();
			baseService.call("POST", "sugestao", request)
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}
        
        function adicionar(request){
            var def = $q.defer();
			baseService.call("POST", "sugestao/adicionar", request)
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
        }
        
        function responder(request){
            var def = $q.defer();
			baseService.call("POST", "sugestao/responder", request)
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
        }
	}
})();