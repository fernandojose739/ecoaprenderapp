(function () {
	'use strict';

	app.service('usuarioService', usuarioService);

	usuarioService.$inject = ['baseService', 
							  '$q', 
							  'escolaService',
							  'localStorageService'];

	function usuarioService(baseService, $q, escolaService, localStorageService) {

		var service = this;

		service.login = login;
		service.listar = listar;
		service.obterDoLocalStorage = obterDoLocalStorage;

		return service;

		function obterDoLocalStorage(){
			if(localStorageService.obter('usuario') == undefined || localStorageService.obter('usuario') == null){
				return;
			};

			var usuario = JSON.parse(localStorageService.obter('usuario'));
			return usuario;
		};

		function login(email, senha) {
			var def = $q.defer();
			baseService.call("GET", "usuario/login/" + email + "/" + senha, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		};

		function listar() {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "usuario/listar/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}
	}
})();