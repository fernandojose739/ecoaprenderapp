(function () {
	'use strict';

	app.service('videoService', VideoService);

	VideoService.$inject = ['baseService', '$q', 'escolaService'];

	function VideoService(baseService, $q, escolaService) {

		var service = this;

		service.ListarTodos = listarTodos;

		return service;

		function listarTodos(pagina) {
			var escola = escolaService.obterEscola();
			var def = $q.defer();
			baseService.call("GET", "video/" + pagina + "/" + escola.Id, "")
				.then(function (data) {
					def.resolve(data);
				}, function (error) {
					def.reject("Error: " + error);
				});
			return def.promise;
		}
	}
})();